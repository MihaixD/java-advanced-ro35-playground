package sample;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SampleCalculatorTest {
    @Test
    void addTest() {
        SampleCalculator calculator = new SampleCalculator();

        assertEquals(0, calculator.add(0, 0));
        assertEquals(2, calculator.add(1, 1));
        assertEquals(8, calculator.add(3, 5));
    }
}