package exercises.task9;

import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {

    @Test
    void getPerimeter() {
        Circle circle = new Circle(new Point2D(0, 0), new Point2D(1, 0));
        assertEquals(2 * Math.PI, circle.getPerimeter(), 0.01);
    }

    @Test
    void getArea() {
        Circle circle = new Circle(new Point2D(0, 0), new Point2D(1, 0));
        assertEquals(Math.PI, circle.getArea(), 0.01);
    }

    @Test
    void getSlicePoints() {
        Circle circle = new Circle(new Point2D(0, 0), new Point2D(1, 0));
        List<Point2D> expectedPoints = List.of(
                new Point2D(0, 1),
                new Point2D(-1, 0),
                new Point2D(0, -1)
        );
        List<Point2D> actualPoints = circle.getSlicePoints();
        assertEquals(3, actualPoints.size());
        //assertTrue(actualPoints.containsAll(expectedPoints));
        org.assertj.core.api.Assertions.assertThat(actualPoints)
                .hasSize(3)
                .containsAll(expectedPoints);

    }

}