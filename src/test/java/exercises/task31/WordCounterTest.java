package exercises.task31;

import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class WordCounterTest {
    @Test
    public void wordCountTest(){
        List<String> inputWords = List.of("Java", "Python", "Java");
        Map<String, Integer> outputWords = WordCounter.wordCount(inputWords);
        Assertions.assertEquals(2,outputWords.size());
        Assertions.assertEquals(2,outputWords.get("Java"));
        Assertions.assertEquals(1,outputWords.get("Python"));
    }

}