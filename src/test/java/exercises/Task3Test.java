package exercises;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;


class Task3Test {

    @Test
    void getMapEntriesAsString() {
        Task3 task3 = new Task3();
        Map<String, Integer> inputMap = Map.of("Java", 18, "Python", 1, "PHP", 0);
        String expectedOutput = "Key: Java, Value: 18\n" +
                "Key: PHP, Value: 0\n" +
                "Key: Python, Value: 1\n";
        assertEquals(expectedOutput, task3.getMapEntriesAsString(inputMap));
    }
}