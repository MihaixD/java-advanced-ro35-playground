package exercises.task23;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ZooTest {

    @Test
    void getNumberOfAllAnimals() {
        Zoo zoo = new Zoo();
        assertEquals(0,zoo.getNumberOfAllAnimals());
        zoo.addAnimals("frog",2);
        zoo.addAnimals("wolf",10);
        zoo.addAnimals("bear",12);
        assertEquals(24,zoo.getNumberOfAllAnimals());
    }
}