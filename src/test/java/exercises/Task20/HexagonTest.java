package exercises.Task20;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HexagonTest {
    Shape hexagon = new Hexagon(2);

    @Test
    void calculateShape() {
        assertEquals(12, hexagon.calculateShape(), 0.01);
    }

    @Test
    void calculateArea() {
        assertEquals(10.39,  hexagon.calculateArea(), 0.01);
    }
}