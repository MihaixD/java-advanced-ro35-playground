package exercises.Task20;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TriangleTest {

    Shape triangle = new Triangle(5);

    @Test
    void calculateShape() {
        assertEquals(15, triangle.calculateShape(), 0.01);

    }

    @Test
    void calculateArea() {
        assertEquals(10.82, triangle.calculateArea(), 0.01);
    }

}