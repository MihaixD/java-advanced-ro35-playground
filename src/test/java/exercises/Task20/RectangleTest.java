package exercises.Task20;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RectangleTest {

    Shape rectangle = new Rectangle(5, 10);

    @Test
    void calculateShape() {
        assertEquals(30 , rectangle.calculateShape(), 0.01);

    }

    @Test
    void calculateArea() {
        assertEquals(50, rectangle.calculateArea(), 0.01);
    }
}