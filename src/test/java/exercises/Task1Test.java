package exercises;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Task1Test {

    @Test
    void sortListAlphabetically() {
        //alreadySortedInitialListTest
        Task1 task1 = new Task1();
        assertEquals(List.of("3", "2", "1"),
                task1.sortListAlphabeticallyReversed(List.of("3", "2", "1")));

        //unsortedInitialListTest
        assertEquals(List.of("3", "2", "1"),
                task1.sortListAlphabeticallyReversed(List.of("1", "3", "2")));

    }
}