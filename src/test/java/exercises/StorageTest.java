package exercises;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StorageTest {

    @Test
    public void findValueAsList (){
        Storage storage = new Storage();

        // Find empty list while storage is empty
        assertEquals(List.of(), storage.findValuesAsList("D"));

        // Find value with one key match
        storage.addToStorage("A", "ABC");
        storage.addToStorage("B", "ACB");

        assertEquals(List.of("A"), storage.findValuesAsList("ABC"));

        storage.addToStorage("E", "ABC");

        assertEquals(List.of("A", "E"), storage.findValuesAsList("ABC"));
    }

}
