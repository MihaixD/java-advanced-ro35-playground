package exercises;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


class Task2Test {
    @Test
    void sortListAlphabeticallyInsensitive(){
        Task2 task2 = new Task2();
        assertEquals(List.of("3", "2", "1"),
                task2.sortListAlphabetically(List.of("3", "2", "1")));
        assertEquals(List.of("e", "B", "A"),
                task2.sortListAlphabetically(List.of("A","e","B")));

    }

}