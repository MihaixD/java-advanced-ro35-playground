package collections;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SDAHashSetTest {
    @Test
    public void testEmptySetHasSizeZero() {
        MySet<String> set = new SDAHashSet<>();

        assertEquals(0, set.size());
    }

    @Test
    public void testSetWithElementsHasCorrectSize() {
        MySet<String> set = new SDAHashSet<>();
        set.add("1");
        set.add("2");
        set.add("3");

        assertEquals(3, set.size());
    }

    @Test
    public void testSetCanFindElementsWhenElementsAreAdded() {
        MySet<String> set = new SDAHashSet<>();
        set.add("12");
        set.add("abc");

        assertTrue(set.contains("12"));
        assertTrue(set.contains("abc"));
    }

    @Test
    public void testSetCanNotFindElementAfterRemoval() {
        MySet<String> set = new SDAHashSet<>();
        set.add("1");
        set.add("2");
        set.add("3");

        assertTrue(set.contains("1"));
        set.remove("1");
        assertFalse(set.contains("1"));
    }


}