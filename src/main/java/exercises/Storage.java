package exercises;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

//task 4
public class Storage {
    private final Map<String, List<String>> storageMap = new HashMap<>();

    public Storage() {

    }

    public void addToStorage(String key, String value) {
        List<String> valuesStringList = storageMap.getOrDefault(key, new ArrayList<>());
        valuesStringList.add(value);
        storageMap.put(key, valuesStringList);
    }

    public void printValues(String key) {
        System.out.println(storageMap.get(key));
    }

    public void findValues(String value) {
        for (String key : storageMap.keySet()) {
            List<String> values = storageMap.get(key);
            if (values.contains(value)) {
                System.out.println(key);
            }
        }

    }

    public List<String> findValuesAsList(String value) {
        return storageMap.keySet().stream()
                .filter(key -> storageMap.get(key).contains(value))
                .collect(toList());
    }

    public List<String> findValuesAsListOld(String value) {
        List<String> result = new ArrayList<>();
        for (String key : storageMap.keySet()) {
            List<String> values = storageMap.get(key);
            if (values.contains(value)) {
                result.add(key);
            }
        }
        return result;
    }

}

