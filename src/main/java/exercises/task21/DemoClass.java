package exercises.task21;

public class DemoClass {
    public static void main(String[] args) {
        ThreeDShape cone = new Cone(1);
        ThreeDShape qube = new Qube(1);

        System.out.println(cone.calculateVolume());
        System.out.println(qube.calculateVolume());
    }
}
