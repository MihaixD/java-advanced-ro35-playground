package exercises.task21;

public class Qube extends ThreeDShape {

    private final double side;

    public Qube (double side){
        this.side = side;
    }

    @Override
    public double calculateVolume() {
        return Math.pow(side, 3);
    }
}
