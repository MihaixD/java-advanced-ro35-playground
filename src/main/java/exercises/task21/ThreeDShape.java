package exercises.task21;

public abstract class ThreeDShape extends Shape {

    public abstract double calculateVolume();

}
