package exercises.task21;

public class Cone extends ThreeDShape {
    private final double radius;

    public Cone(double radius) {
        this.radius = radius;
    }

    @Override
    public double calculateVolume() {
        float pi = 3.14f;
        return (4 * Math.PI * Math.pow(radius, 3) / 3);
    }
}
