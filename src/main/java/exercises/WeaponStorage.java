package exercises;

public class WeaponStorage {
    private String bulletText = null;
    private boolean loaded = false;

    public void loadBullet (String bulletText){
        this.bulletText= bulletText;
        this.loaded=true;
    }
    public boolean isLoaded (){
        return loaded;
    }
   public void shoot(){
        if(loaded){
            System.out.println("Can shoot " + bulletText);
        }

   }

    public static void main(String[] args) {

        WeaponStorage weapon = new WeaponStorage();
        System.out.println(weapon.isLoaded());
        weapon.loadBullet("pew");
        System.out.println(weapon.isLoaded());
        weapon.shoot();

    }
}
