package exercises.Task20;

public class Hexagon extends Shape {

    double l;

    public Hexagon(double l) {
        this.l = l;
    }


    @Override
    public double calculateShape() {
        return 6 * l;
    }

    @Override
    public double calculateArea() {
        return Math.pow(l, 2) * (3 * Math.sqrt(3) / 2);
    }
}
