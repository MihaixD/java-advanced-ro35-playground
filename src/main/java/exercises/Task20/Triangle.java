package exercises.Task20;

public class Triangle extends Shape {

    // triunghi echilateral

    double l;


    public Triangle(double l) {
        this.l = l;

    }

    @Override
    public double calculateShape() {
        return 3 * l;
    }

    @Override
    public double calculateArea() {
        return (Math.pow(l, 2) * Math.sqrt(3)) / 4;
    }
}
