package exercises;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class Task3 {
    public void printMapEntries(Map<String, Integer> map) {
        for (String key : map.keySet()) {
            Integer value = map.get(key);
            System.out.println("Key: " + key + ", Value: " + value);


        }
    }

    public String getMapEntriesAsString(Map<String, Integer> map) {
        StringBuilder result = new StringBuilder();
        List<String> orderedKeys = map.keySet().stream()
                .sorted()
                .collect(toList());
        for (String key : orderedKeys) {
            Integer value = map.get(key);
            result.append("Key: ").append(key).append(", Value: ").append(value).append("\n");
        }
        return result.toString();
    }

}
