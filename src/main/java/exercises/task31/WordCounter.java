package exercises.task31;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

public class WordCounter {

    public static Map<String, Integer> wordCount(List<String> words) {
        Map<String, Integer> countByWord = new HashMap<>();
        for (String word : words) {
            Integer currentCount = countByWord.getOrDefault(word, 0);
            countByWord.put(word, currentCount + 1);
        }
        return countByWord;
    }

    public static List<String> readWordsFromFile(Path file) {
        try {
            return Files.readAllLines(file).stream()
                    .flatMap(line -> Arrays.stream(line.split(" ")))
                    .collect(Collectors.toList());

        } catch (IOException e) {
            System.out.println("Cannot read from file");
        }
        return new ArrayList<>();
    }
    public void writeWordCountToFile(Map<String, Integer> map, Path file){
        try {
            Files.write(file, map.toString().getBytes(StandardCharsets.UTF_8),
                    StandardOpenOption.WRITE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

