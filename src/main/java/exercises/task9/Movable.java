package exercises.task9;

public interface Movable {
    void move(MoveDirection moveDirection);
}
