package exercises.task9;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class Point2D implements Movable {
    private double x;
    private double y;

    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Point2D minus(Point2D otherPoint) {
        return new Point2D(this.getX() - otherPoint.getX(), this.getY() - otherPoint.getY());
    }

    public Point2D add(Point2D otherPoint) {
        return new Point2D(otherPoint.getX() + this.getX(), otherPoint.getY() + this.getY());
    }

    public Point2D rotation(double degrees) {
        double rotationX = Math.cos(Math.toRadians(degrees));
        double rotationY = Math.sin(Math.toRadians(degrees));
        double newX = this.x * rotationX - this.y * rotationY;
        double newY = this.x * rotationY + this.y * rotationX;
        return new Point2D(round(newX), round(newY));
    }

    private static double round(double value) {
        return new BigDecimal(value)
                .setScale(3, RoundingMode.HALF_EVEN)
                .doubleValue();
    }


    @Override
    public void move(MoveDirection moveDirection) {
        this.x += moveDirection.getX();
        this.y += moveDirection.getY();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point2D point2D = (Point2D) o;
        return Double.compare(point2D.x, x) == 0 && Double.compare(point2D.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Point2D{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
