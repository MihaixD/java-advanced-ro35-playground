package exercises.task9;

import java.util.ArrayList;
import java.util.List;

public class Circle implements Movable {
    private final Point2D center;
    private final Point2D point;

    public Circle(Point2D center, Point2D point) {
        this.center = center;
        this.point = point;
    }

    public double getRadius() {
        return Math.sqrt(Math.pow((point.getX() - center.getX()), 2) +
                Math.pow((point.getY() - center.getY()), 2));
    }

    public double getPerimeter() {
        return 2 * Math.PI * this.getRadius();
    }

    public double getArea() {
        return Math.PI * Math.pow(this.getRadius(), 2);
    }

    public List<Point2D> getSlicePoints() {
        //separate the distance between center and point as another point
        List<Point2D> result = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            Point2D slicePoint = point.minus(center);
            //apply 90 degrees rotation  then add to circle
            Point2D rotation = slicePoint.rotation(i * 90);
            result.add(center.add(rotation));

        }
        return result;
    }


    @Override
    public void move(MoveDirection moveDirection) {
        this.center.move(moveDirection);
        this.point.move(moveDirection);
    }
}
