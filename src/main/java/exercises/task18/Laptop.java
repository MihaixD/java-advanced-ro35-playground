package exercises.task18;

public class Laptop extends Computer {
    private String battery;
    
    public Laptop(String processor, double ram, String graphicCard, String company, String model, String battery) {
        super(processor, ram, graphicCard, company, model);
        this.battery = battery;
    }
    
    public String getBattery() {
        return battery;
    }
    
    public void setBattery(String battery) {
        this.battery = battery;
    }
    
    @Override
    public String toString() {
        Computer computer = new Laptop("Intel", 16, "GIGABYTE GeForce", "GIGABYTE", " RTX 2060 D6 6G (rev. 2.0)", "Litiu-Polymer");
        return "Laptop{" +
                "processor='" + computer.getProcessor() + '\'' +
                ", ram=" + computer.getRam() +
                ", graphicCard='" + computer.getGraphicCard() + '\'' +
                ", company='" + computer.getCompany() + '\'' +
                ", model='" + computer.getModel() + '\'' +
                "battery='" + battery + '\'' +
                '}';
    }
}
