package exercises.task18;

public class ComputerDemo {
    public static void main(String[] args) {
        Computer computer1 = new Computer("AMD", 8, "NVidia", "Dell", "5500 Series");
        System.out.println(computer1);
        Computer computer2 = new Computer("Intel", 16, "GForce", "Apple", "Some model");
        System.out.println(computer2);
    }
}
