package exercises.task18;

import java.util.Objects;

public class Computer {
    private String processor;
    private double ram;
    private String graphicCard;
    private String company;
    private String model;
    
    public Computer(String processor, double ram, String graphicCard, String company, String model) {
        this.processor = processor;
        this.ram = ram;
        this.graphicCard = graphicCard;
        this.company = company;
        this.model = model;
    }
    
    public String getProcessor() {
        return processor;
    }
    
    public double getRam() {
        return ram;
    }
    
    public String getGraphicCard() {
        return graphicCard;
    }
    
    public String getCompany() {
        return company;
    }
    
    public String getModel() {
        return model;
    }
    
    public void setProcessor(String processor) {
        this.processor = processor;
    }
    
    public void setRam(double ram) {
        this.ram = ram;
    }
    
    public void setGraphicCard(String graphicCard) {
        this.graphicCard = graphicCard;
    }
    
    public void setCompany(String company) {
        this.company = company;
    }
    
    public void setModel(String model) {
        this.model = model;
    }
    
    @Override
    public String toString() {
        return "Computer{" + "processor='" + processor + '\'' + ", ram=" + ram + ", graphicCard='" + graphicCard + '\'' + ", company='" + company + '\'' + ", model='" + model + '\'' + '}';
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Computer computer = (Computer) o;
        return ram == computer.ram && Objects.equals(processor, computer.processor) && Objects.equals(graphicCard, computer.graphicCard) && Objects.equals(company, computer.company) && Objects.equals(model, computer.model);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(processor, ram, graphicCard, company, model);
    }
}
