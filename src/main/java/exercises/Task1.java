package exercises;

import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

//Create
//a method that takes a string list as a parameter ,
// then returns the list sorted alphabetically from Z to A.
public class Task1 {

    public List<String> sortListAlphabeticallyReversed(List<String> list) {
        return list.stream()
                .sorted(Comparator.reverseOrder())
                .collect(toList());
    }
}

