package exercises.task32;

import lombok.Setter;

import java.io.Serializable;

@Setter
public class Car {
    private String name;
    private int yearOfFabrication;
    private String color;

    public Car(){

    }

    public Car(String name, int yearOfFabrication, String color) {
        this.name = name;
        this.yearOfFabrication = yearOfFabrication;
        this.color = color;
    }
    public static Car createCarFromString(String[] strings) {
        Car car = new Car();
        car.setName(strings[0]);
        car.setYearOfFabrication(Integer.parseInt(strings[1]));
        car.setColor(strings[2]);
        return car;
    }

    @Override
    public String toString() {
        return "Car:" +
                 name +
                "," + yearOfFabrication +
                "," + color +
                "\n";
    }
}
