package exercises.task32;

import java.util.List;

public class DemoTask32 {
    public static void main(String[] args) {
        List<Car> cars = List.of(
                new Car("Audi",2015,"black"),
                new Car("Dacia",2020,"red"),
                new Car("Ford",2018,"white")
        );
        Exercise32.saveItemsToFile(cars,"C:\\JavaProjects\\java-advanced-ro35-playground\\src\\main\\resources\\cars.txt");

        List<Car> carsFromFile = Exercise32.loadFromFile("C:\\JavaProjects\\java-advanced-ro35-playground\\src\\main\\resources\\exercise32.txt");
        for(Car car:carsFromFile){
            System.out.println(car);
        }
    }
}
