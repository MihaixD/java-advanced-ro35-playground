package exercises.task32;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import static exercises.task32.Car.createCarFromString;

public class Exercise32 {

    public static void saveItemsToFile(List<Car> cars, String path) {
        try {
            for (Car car : cars) {
                Files.write(Paths.get(path), car.toString().getBytes(), StandardOpenOption.CREATE,StandardOpenOption.APPEND);
            }

        } catch (IOException e) {
            System.out.println("Error writing");
        }
    }

    public static List<Car> loadFromFile(String path) {
        List<Car> result = new ArrayList<>();
        try {
            List<String> lines = Files.readAllLines(Paths.get(path));
            for (String line : lines) {
                String[] strings = line.substring(4).split(",");
                Car car = createCarFromString(strings);
                result.add(car);
            }
        } catch (IOException e) {
            System.out.println("Error reading file");
        }
        return result;
    }


}
