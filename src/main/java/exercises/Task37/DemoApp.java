package exercises.Task37;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DemoApp {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        executorService.submit(new ThreadPlayGroundRunnable("Nume"));
        executorService.submit(new ThreadPlayGroundRunnable("George"));
        executorService.submit(new ThreadPlayGroundRunnable("Andrei"));
        executorService.submit(new ThreadPlayGroundRunnable("Maria"));
        executorService.submit(new ThreadPlayGroundRunnable("Alex"));
        executorService.shutdown();
    }
}
