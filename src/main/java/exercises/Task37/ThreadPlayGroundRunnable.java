package exercises.Task37;

public class ThreadPlayGroundRunnable implements Runnable {
    private String name;

    public ThreadPlayGroundRunnable(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(name + " " + Thread.currentThread().getName());
        }
    }
}
