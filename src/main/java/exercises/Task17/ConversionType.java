package exercises.Task17;
//Create a ConversionType enum class with the constants METERS_TO_YARDS, YARDS_TO_METERS,
        //CENTIMETERS_TO_ICHES, INCHES_TO_CENTIMETERS, KILOMETERS_TO_MILES, MILES_TO_KILOMETERS.
        //Enum should have a Convertertype parameter used to perform calculations for a given type.

public enum ConversionType {
    METERS_TO_YARDS (value -> value*1.09),
    YARDS_TO_METERS(value -> value*0.91),
    CENTIMETERS_TO_INCHES(value -> value*0.39),
    INCHES_TO_CENTIMETERS(value ->value*2.54 ),
    KILOMETERS_TO_MILES(value ->value*0.62),
    MILES_TO_KILOMETERS(value ->value*1.6);

    private Converter converter;

    ConversionType(Converter converter){
        this.converter=converter;
    }

    public Converter getConverter() {
        return converter;
    }
}

