package exercises.Task17;

@FunctionalInterface
public interface Converter {
    double convert(double value);
}
