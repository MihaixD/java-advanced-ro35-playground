package exercises.Task17;
//Then create a MeasurementConverter class that will have the convert(int value, ConvertionType
//) method and based on the value and type of conversion, used the Converter of the given
        //type and returned the result.
public class MeasurementConverter {
    public double  convert(double value, ConversionType conversionType ){

        return conversionType.getConverter().convert(value);
    }
}
