package exercises.task24;

public class BasketDemo {
    public static void main(String[] args) {
        Basket basket = new Basket();
        try {
            basket.addToBasket();
            basket.removeFromBasket();
            basket.removeFromBasket();
        }catch(BasketEmptyException myException){
            System.out.println("Caught basket exception");
        }
        System.out.println("Program still works");
    }
}
