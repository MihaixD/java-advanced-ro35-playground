package exercises.task24;

public class Basket {
    private int count = 0;

    public void addToBasket() {
        if(count >= 10){
            throw new BasketFullException();
        }
        count++;

    }

    public void removeFromBasket() throws BasketEmptyException {
        if(count == 0){
            throw new BasketEmptyException();
        }
        count--;
    }

}

