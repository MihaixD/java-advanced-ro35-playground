package exercises;

import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class Task2 {
    private static final Comparator<String> ALPHABETICAL_ORDER_IGNORECASE = new SortAlphabetically();

    public List<String> sortListAlphabetically(List<String> list) {
        return list.stream()
                .sorted(ALPHABETICAL_ORDER_IGNORECASE)
                .collect(toList());

    }

    private static class SortAlphabetically implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            return o2.compareToIgnoreCase(o1);
        }
    }
}

