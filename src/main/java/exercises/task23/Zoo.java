package exercises.task23;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class Zoo {
    private Map<String, Integer> animals = new HashMap<>();

    public int getNumberOfAllAnimals() {
        return animals.values()
                .stream().mapToInt(Integer::intValue).sum();
    }

    public Map<String, Integer> getAnimalsCount() {
        return animals;
    }

    public Map<String, Integer> getAnimalsCountSorted() {
        return animals.entrySet()
                .stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue((o1, o2) -> o2 - o1))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

    public void addAnimals(String animal, int number) {
            int actualNumber = animals.getOrDefault(animal,0);
            animals.put(animal, actualNumber + number);

    }

}
