package exercises.task23;

public class DemoZoo {
    public static void main(String[] args) {
        Zoo zoo = new Zoo();
        zoo.addAnimals("elephant",2);
        zoo.addAnimals("lion",4);
        zoo.addAnimals("bear",5);
        zoo.addAnimals("snake",6);
        System.out.println(zoo.getAnimalsCount());
        System.out.println(zoo.getNumberOfAllAnimals());
        System.out.println(zoo.getAnimalsCountSorted());
    }
}
