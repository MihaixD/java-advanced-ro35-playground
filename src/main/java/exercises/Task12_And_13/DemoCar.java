package exercises.Task12_And_13;

import java.util.ArrayList;
import java.util.List;

public class DemoCar {
    public static void main(String[] args) {
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Car car4 = new Car();
        car1.setName("Toyota");
        car2.setName("bmw");
        car3.setName("audi");
        car4.setName("ford");
        CarService serviceSandra = new CarService();
        serviceSandra.addCar(car1);
        serviceSandra.addCar(car2);
        serviceSandra.addCar(car3);
        serviceSandra.addCar(car4);
        car1.setType(EngineType.V12);
        car2.setType(EngineType.V6);
        car3.setType(EngineType.V12);
        car4.setType(EngineType.V8);
        List<Car> v12Cars = serviceSandra.getCarsByEngineType(EngineType.V12);
        System.out.println(v12Cars);

        Manufacturer manufacturer = new Manufacturer("BMW", "1920","Germany");
        List<Manufacturer> manufacturerList = new ArrayList<>();
        manufacturerList.add(manufacturer);
        car1.setManufacturerList(manufacturerList);

        Car car5 = new Car.CarBuilder()
                .manufacturerList(manufacturerList)
                .model("V40")
                .yearOfManufacture(1995)
                .build();

    }



}
