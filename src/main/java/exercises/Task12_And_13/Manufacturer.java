package exercises.Task12_And_13;

import java.util.Objects;

public class Manufacturer {
    String manufacturerName;
    String yearOfEstablishment;
    String country;

    public Manufacturer(String manufacturerName, String yearOfEstablishment, String country) {
        this.manufacturerName = manufacturerName;
        this.yearOfEstablishment = yearOfEstablishment;
        this.country = country;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public String getYearOfEstablishment() {
        return yearOfEstablishment;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Manufacturer that = (Manufacturer) o;
        return Objects.equals(manufacturerName, that.manufacturerName) && Objects.equals(yearOfEstablishment,
                that.yearOfEstablishment) && Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(manufacturerName, yearOfEstablishment, country);
    }
}
