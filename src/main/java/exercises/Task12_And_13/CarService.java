package exercises.Task12_And_13;

import java.util.*;
import java.util.stream.Collectors;

public class CarService {
    List<Car> cars = new ArrayList<>();

    public void addCar(Car car) {
        cars.add(car);
    }

    public void removeCar(Car car) {
        cars.remove(car);
    }

    public List<Car> getCars() {
        return cars;
    }

    public List<Car> getCarsByEngineType(EngineType engineType) {
        return cars.stream()
                .filter(car -> car.getType() == engineType)
                .collect(Collectors.toList());
    }

    public List<Car> getCarsProducedAfter(int year) {
        return cars.stream()
                .filter(car -> car.getYearOfManufacture() > year)
                .collect(Collectors.toList());
    }

    public Optional<Car> getMostExpensiveCar() {
        return cars.stream()
                .max(Comparator.comparing(Car::getPrice));
    }

    public Optional<Car> getCheapestCar() {
        return cars.stream()
                .min(Comparator.comparing(Car::getPrice));
    }

    public List<Car> atLeastGivenAmountOfManufactures(int numberOfManufactures) {
        return cars.stream()
                .filter(car -> car.getManufacturerList().size() > numberOfManufactures)
                .collect(Collectors.toList());
    }

    // this can be refectored using polymorphism (see example from task 17)
    public List<Car> sortedBy(String fieldName, String orderingType) {

        if (Objects.equals(fieldName, "years") && Objects.equals(orderingType, "ASC")) {
            return cars.stream()
                    .sorted(Comparator.comparingInt(Car::getYearOfManufacture))
                    .collect(Collectors.toList());
        }
        if (Objects.equals(fieldName, "price") && Objects.equals(orderingType, "DESC")) {
            return cars.stream()
                    .sorted(Comparator.comparingDouble(Car::getPrice).reversed())
                    .collect(Collectors.toList());
        }
        if (Objects.equals(fieldName, "price") && Objects.equals(orderingType, "ASC")) {
            return cars.stream()
                    .sorted(Comparator.comparingDouble(Car::getPrice))
                    .collect(Collectors.toList());
        }
        if (Objects.equals(fieldName, "years") && Objects.equals(orderingType, "DESC")) {
            return cars.stream()
                    .sorted(Comparator.comparingInt(Car::getYearOfManufacture).reversed())
                    .collect(Collectors.toList());
        }


        throw new UnsupportedOperationException("Given sort parameters not supported "
                + fieldName + ", " + orderingType);
    }

    public boolean contains(Car car) {
        return cars.contains(car);
    }

    public List<Car> getCarsBySpecifiedManufacturer(Manufacturer manufacturer) {
        return cars.stream()
                .filter(car -> car.getManufacturerList().contains(manufacturer))
                .collect(Collectors.toList());

    }

    public List<Car> getCarsByManufacturerYear(String year) {
        return cars.stream()
                .filter(car -> anyManufacturerMatchesYear(car, year))
                .collect(Collectors.toList());
    }

    private boolean anyManufacturerMatchesYear(Car car, String year) {
        return car.getManufacturerList()
                .stream()
                .anyMatch(manufacturer -> Objects.equals(manufacturer.getYearOfEstablishment(), year));
    }


    @Override
    public String toString() {
        return "CarService{" +
                "cars=" + cars +
                '}';
    }
}

