package exercises.Task12_And_13;

import lombok.*;

import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Builder

public class Car {
    String name;
    String model;
    double price;
    int yearOfManufacture;
    List<Manufacturer> manufacturerList;
    EngineType type;

    public Car(String name, String model, double price, int yearOfManufacture, List<Manufacturer> manufacturerList, EngineType type) {
        this.name = name;
        this.model = model;
        this.price = price;
        this.yearOfManufacture = yearOfManufacture;
        this.manufacturerList = manufacturerList;
        this.type = type;
    }

    public Car() {

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Double.compare(car.price, price) == 0 && Objects.equals(name, car.name) && Objects.equals(model, car.model) && Objects.equals(yearOfManufacture, car.yearOfManufacture) && Objects.equals(manufacturerList, car.manufacturerList) && type == car.type;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", price=" + price +
                ", yearOfManufacture='" + yearOfManufacture + '\'' +
                ", list=" + manufacturerList +
                ", type=" + type +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, model, price, yearOfManufacture, manufacturerList, type);
    }
}
