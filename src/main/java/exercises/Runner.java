package exercises;

/*Create an enum Runner class with constants BEGINNER, INTERMEDIATE, ADVANCED.
Enum should have two parameters:
        • minimum time of running a marathon in minutes
        • maximum running time of the marathon in minutes
In addition, the Runner enum should contain the static getFitnessLevel() method, which takes any time
result of a marathon run, and as a result should return a specific Runner object based on the given time.*/

public enum Runner {
    BEGINNER(1,59),
    INTERMEDIATE(60,179),
    ADVANCED(180,1000);

    private int minTimeOfRunning;
    private int maxTimeOfRunning;

    Runner(int minTimeOfRunning, int maxTimeOfRunning){
        this.minTimeOfRunning = minTimeOfRunning;
        this.maxTimeOfRunning = maxTimeOfRunning;
    }

    private static Runner getFitnessLevel(int minTimeOfRunning) {
        if (minTimeOfRunning > ADVANCED.minTimeOfRunning) {
            return ADVANCED;
        } else if (minTimeOfRunning > INTERMEDIATE.minTimeOfRunning) {
            return INTERMEDIATE;
        }
        return BEGINNER;
    }


}
