package task8;

public class DemoApp {
    public static void main(String[] args) {
        Parcel parcel = new Parcel();
        parcel.zLength = 65;
        parcel.xLength = 38;
        parcel.yLength = 92;

        parcel.weight = 5;

        boolean result = parcel.validate();
        System.out.println(result);


        parcel.zLength = 165;
        parcel.xLength = 138;
        parcel.yLength = 192;
        boolean result2 = parcel.validate();
        System.out.println(result2);

    }
}
