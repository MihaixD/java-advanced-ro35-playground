package task8;

public class Parcel implements Validator {
    int xLength;
    int yLength;
    int zLength;
    float weight;
    boolean isExpress;

    @Override
    public boolean validate() {
        return verifySum() && verifyWeight() && verifyEachSize();
    }

    private boolean verifySum() {
        return xLength + yLength + zLength < 300;
    }

    private boolean verifyWeight() {
        if (weight < 30.0 && !isExpress) {
            return true;
        }
        return weight < 15.0 && isExpress;
    }

    private boolean verifyEachSize() {
        return xLength > 30 && yLength > 30 && zLength > 30;
    }
}
