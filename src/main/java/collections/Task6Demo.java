package collections;

import java.util.TreeMap;

public class Task6Demo {
    public static void main(String[] args) {
        TreeMap<String, String> treeMap = new TreeMap<>();
        treeMap.put("3", "anm");
        treeMap.put("1", "abd");
        treeMap.put("2", "ald");
        treeMap.put("4", "cdabd");
        printMapEntries(treeMap);
        
    }
    
     public static void printMapEntries(TreeMap<String, String> map) {
        System.out.println(map.firstEntry());
        System.out.println(map.lastEntry());
    }
}
