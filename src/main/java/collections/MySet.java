package collections;

public interface MySet<E> {
    void add(E element);

    void remove(E element);

    int size();

    boolean contains(E element);

    void clear();
}
