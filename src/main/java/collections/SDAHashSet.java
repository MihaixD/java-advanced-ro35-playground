package collections;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SDAHashSet<E> implements MySet<E> {
    List<Bucket<E>> buckets;
    int size = 0;
    int capacity = 8;
    double overloadFactor = 0.75;

    public SDAHashSet() {
        buckets = new ArrayList<>(8);
        initializeEmptyBuckets();
    }

    @Override
    public void add(E element) {
        if (size >= capacity * overloadFactor) {
            resizeBucketsUp();
        }
        int bucketId = getBucketId(element);
        buckets.get(bucketId).add(element);
        size++;
    }

    @Override
    public void remove(E element) {
        int bucketId = getBucketId(element);
        buckets.get(bucketId).remove(element);
        size--;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean contains(E element) {
        int bucketId = getBucketId(element);
        return buckets.get(bucketId).contains(element);
    }

    @Override
    public void clear() {
        buckets.forEach(Bucket::clear);
        size = 0;
    }

    private int getBucketId(E element) {
        int hashCode = element.hashCode();
        return hashCode % capacity;
    }

    private void resizeBucketsUp() {
        List<E> allElements = buckets.stream()
                .flatMap(bucket -> bucket.getBucketItems().stream())
                .collect(Collectors.toList());

        this.clear();
        capacity = 2 * capacity;
        buckets = new ArrayList<>(capacity);
        initializeEmptyBuckets();
        allElements.forEach(this::add);
    }

    private void initializeEmptyBuckets() {
        for (int bucketId = 0; bucketId < capacity; bucketId++) {
            buckets.add(new Bucket<>());
        }
    }

    private static class Bucket<E> {
        private final List<E> bucketItems = new ArrayList<>();

        public void add(E element) {
            bucketItems.add(element);
        }

        public void remove(E element) {
            bucketItems.remove(element);
        }

        public boolean contains(E element) {
            return bucketItems.contains(element);
        }

        public List<E> getBucketItems() {
            return bucketItems;
        }

        public void clear() {
            bucketItems.clear();
        }
    }
}
