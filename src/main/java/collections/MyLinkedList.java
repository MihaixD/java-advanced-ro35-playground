package collections;

import java.util.Objects;

public class MyLinkedList implements MyList {
    private MyNode headNode = null;
    private int size = 0;

    @Override
    public void add(String element) {
        MyNode node = MyNode.of(element);
        if (headNode == null) {
            headNode = node;
        } else {
            headNode.append(node);
        }
        size++;
    }

    @Override
    public String get(int index) {
        int position = 0;
        for (MyNode currentNode = headNode; currentNode != null; currentNode = currentNode.nextNode) {
            if (position == index) {
                return currentNode.elementValue;
            }
            position++;
        }
        return null;
    }

    @Override
    public void remove(String element) {
        if (MyNode.hasElementValue(headNode, element)) {
            headNode = headNode.nextNode;
        }
        for (MyNode currentNode = headNode; currentNode != null; currentNode = currentNode.nextNode) {
            if (MyNode.hasElementValue(currentNode.nextNode, element)) {
                currentNode.nextNode = currentNode.nextNode.nextNode;
            }
        }
        size--;
    }

    @Override
    public int size() {
        return size;
    }

    private static class MyNode {
        String elementValue;
        MyNode nextNode;

        private static MyNode of(String elementValue) {
            MyNode node = new MyNode();
            node.elementValue = elementValue;
            node.nextNode = null;
            return node;
        }

        public void append(MyNode node) {
            if (this.nextNode == null) {
                this.nextNode = node;
            } else {
                this.nextNode.append(node);
            }
        }

        private static boolean hasElementValue(MyNode currentNode, String element) {
            if (currentNode == null) {
                return false;
            }
            return Objects.equals(currentNode.elementValue, element);
        }
    }
}
