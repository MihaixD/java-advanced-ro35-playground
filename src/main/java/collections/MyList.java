package collections;

public interface MyList {
    void add(String element);

    String get(int index);

    void remove(String element);

    int size();
}
