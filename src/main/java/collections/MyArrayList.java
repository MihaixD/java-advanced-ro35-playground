package collections;

import java.util.Arrays;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

public class MyArrayList implements MyList {
    private String[] array = new String[4];
    private int capacity = array.length;
    private int size = 0;

    @Override
    public void add(String element) {
        if (size == capacity) {
            resizeUpArray();
        }
        array[size] = element;
        size++;
    }

    @Override
    public String get(int index) {
        return array[index];
    }

    @Override
    public void remove(String element) {
        int index = findIndexOf(element);
        if (index == -1) {
            return;
        }
        shiftLeft(index);
        if (size < 0.3 * capacity) {
            resizeDownArray();
        }

    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public String toString() {
        return Arrays.stream(array, 0, size).collect(toList()).toString();
    }

    private void shiftLeft(int fromIndex) {
        for (int i = fromIndex; i + 1 < size; i++) {
            array[i] = array[i + 1];
        }
        size--;

    }

    private int findIndexOf(String element) {
        for (int i = 0; i < size; i++) {
            if (Objects.equals(array[i], element)) {
                return i;
            }
        }
        return -1;
    }

    private void resizeUpArray() {
        System.out.println("Resizing array UP from capacity = " + capacity);
        capacity = capacity * 2;
        array = Arrays.copyOf(array, capacity);
    }

    private void resizeDownArray() {
        System.out.println("Resizing array DOWN from capacity = " + capacity);
        capacity = capacity / 2;
        array = Arrays.copyOf(array, capacity);
    }
}
