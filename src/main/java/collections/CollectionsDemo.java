package collections;

import java.util.*;
import java.util.stream.IntStream;

public class CollectionsDemo {
    List<String> list2 = new LinkedList<>();

    Set<String> set = new HashSet<>();
    Set<String> set2 = new TreeSet<>();

    Map<String, String> map = new HashMap<>();
    Map<String, String> map2 = new TreeMap<>();

    public static void main(String[] args) {
        //testMyArrayList();
        //testMyLinkedList();
        testMaps();
    }

    private static void testMaps() {
        Map<String, String> map = new TreeMap<>();
        map.put("key1", "val1");
        map.put("key1", "something else");
        map.put("A", "x");
        map.put("D", "x");
        map.put("V", "x");
        map.put("Aa", "x");

        System.out.println(map);
        Map<String, String> hashMap = new HashMap<>(map);
        System.out.println(hashMap);

        //Map<String, Person> personByCNP = new HashMap<>();
        //Map<String, User> userByEmailId = new HashMap<>();
        //userByEmailId.get("user@domain.com");

        // 100 users -> search all in list -> 100 checks
        // 100 users -> get from hash map -> 1 check
        // 128 users -> tree map -> 1 check per "halving" the set of options
        // 128 -> 64 -> 32 -> 16 -> 8 -> 4 -> 2 -> 1

    }

    private static void testMyLinkedList() {
        MyList myList = new MyLinkedList();
        List.of("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K")
                .forEach(myList::add);
        System.out.println("List has size: " + myList.size());

        List.of("A", "B", "C", "D", "E", "F", "G", "H")
                .forEach(myList::remove);
        System.out.println("List has size: " + myList.size());
    }

    private static void testMyArrayList() {
        MyList myList = new MyArrayList();
        List.of("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K")
                .forEach(myList::add);

        System.out.println(myList);
        List.of("A", "B", "C", "D", "E", "F", "G", "H")
                .forEach(myList::remove);

        System.out.println(myList);

        performLoadTestOnMyArrayList(myList);
    }

    private static void performLoadTestOnMyArrayList(MyList myList) {
        IntStream.range(0, 10000)
                .forEach(i -> myList.add(i + ""));

        IntStream.range(0, 10000)
                .forEach(i -> myList.remove(i + ""));
    }
}
